const cacheFunction = (callback) => {
  let cacheObject = {};
  return function cache(...completeArguments) {
    let key='';
    if(completeArguments.length <=0 ) return ;
    for (let args of completeArguments) {
      key += args.toString();
    }
    if (cacheObject.hasOwnProperty(key)) {
        console.log('returning from cacheobject')
        //console.log(cacheObject);
      return cacheObject[key];
    } else {
      let ans = callback(completeArguments);
      cacheObject[key] = ans;
      console.log('returning from callback function');
      return cacheObject[key];
    }
  };
};

module.exports = cacheFunction;
