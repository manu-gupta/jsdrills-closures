const limitFuncitonCallCount = (callback, number) => {
  let counter = 0;
  //if(!callback || !number) return null;
  return function limit() {
    if (counter < number) {
        counter += 1;
      return callback(counter);
    }
    return null;
  };
};

module.exports = limitFuncitonCallCount;
