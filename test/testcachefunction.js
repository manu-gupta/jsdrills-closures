const cacheFunction = require("../cachefunction");

const returnedCacheFunction = cacheFunction((args) => {
  return args.reduce((total , item)=> total+item);
});
console.log(returnedCacheFunction(1, 2, 3, 4, 3, 6));
console.log(returnedCacheFunction("a", "b", "c", "d", "e"));
console.log(returnedCacheFunction(1, 2, 3, 4, 5, 6));
console.log(returnedCacheFunction(1, 2, 3, 4, 5, 6));
console.log(returnedCacheFunction(1, 2, 3, 4, 5, 6));
console.log(returnedCacheFunction());
