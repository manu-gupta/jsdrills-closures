const counterFactory = (counter) => {
       if(!counter) return ; 
   function increment () {
      counter +=1;
      return counter;
  }
  function decrement (){
      counter -=1;
      return counter;
  }

  return {increment , decrement };

}

module.exports = counterFactory;

